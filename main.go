package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
)

func main() {
	args := os.Args[1:]
	if len(args) != 2 {
		log.Fatalln("wrong number of args")
	}
	out := generate(getInput(args[0]))
	file, err := os.Create(args[1])
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	_, err = file.WriteString(out)
	if err != nil {
		log.Fatal(err)
	}
}

func getInput(path string) []string {
	infos, err := os.ReadDir(path)
	if err != nil {
		log.Fatalln(err)
	}
	files := make([]string, 0)
	for _, info := range infos {
		if info.IsDir() {
			return nil
		}
		filename := filepath.Join(path, info.Name())
		b, err := os.ReadFile(filename)
		if err != nil {
			fmt.Println("file " + filename + " cannot be read because of " + err.Error())
			continue
		}
		files = append(files, string(b))
	}
	return files
}

func generate(input []string) string {
	ret := `\documentclass[a5paper]{article}
\usepackage{hyperref}
\usepackage{lmodern}
\usepackage{setspace}
\doublespacing
\setlength\parindent{0pt}
\usepackage{geometry}
 \geometry{
 a5paper,
 left=20mm,
 top=20mm,
 }
\begin{document}
`

	for i, song := range input {
		ret += generateSingle(song)
		if i != len(input)-1 {
			ret += `\newpage` + "\n"
		}
	}
	ret += `\end{document}`
	return ret
}

func generateSingle(in string) string {
	if in == "" {
		return ""
	}

	in = strings.Replace(in, "#", `\#`, -1)

	lines := strings.Split(in, "\n")
	ret := `\section{` + lines[0] + "}\n"
	if len(lines) == 1 {
		return ret
	}
	state := 0
	for i := 1; i < len(lines); i++ {
		line := lines[i]
		if state != 0 && line == "" {
			if line == "" {
				ret += `\medskip` + "\n\n"
				continue
			}
		}
		if state != 1 && line == "text:" {
			ret += `\begin{singlespace}` + "\n"
			state = 1
			continue
		}
		if state != 2 && line == "chords:" {
			if state == 1 {
				ret += `\end{singlespace}` + "\n"
			}
			state = 2
			continue
		}
		switch state {
		case 1:
			ret += line + "\n\n"
		case 2:
			ret += parseChordLine(line) + "\n\n"
		}
	}
	if state == 1 {
		ret += `\end{singlespace}` + "\n"
	}
	return ret
}

func getChordBar(chord string, dynamicSize bool) string {
	if dynamicSize {
		return `\rule[-1ex]{1sp}{4ex}\raisebox{2ex}[0pt][0pt]{ {\fontsize{6}{10}\selectfont \textbf{` + chord + `}}}`
	} else {
		return `\rule[-1ex]{1sp}{4ex}\raisebox{2ex}[0pt][0pt]{\makebox[1.5cm][l]{ {\fontsize{6}{10}\selectfont \textbf{` + chord + `}}}}\hspace*{-1.5cm} `
	}
}

func getSmallBar() string {
	return `\rule[-1ex]{1sp}{0.7ex}`
}

type token struct {
	t    byte // 0 - dot, 1 - chord, 2 - word
	text string
}

func parseChordLine(line string) string {
	tokens := make([]token, 0)
	for _, word := range strings.Split(line, " ") {
		if word == "" {
			continue
		}
		if word == ";" {
			tokens = append(tokens, token{0, getSmallBar()})
			continue
		}
		if chord, ok := isChord(word); ok {
			// the resulting text of chordBars between words must to be decided after
			tokens = append(tokens, token{1, chord})
			continue
		}
		tokens = append(tokens, token{2, parseChordWord(word)})
	}

	ret := ""
	for i := 0; i < len(tokens)-1; i++ {
		t1, t2 := tokens[i], tokens[i+1]
		// deciding resulting text of the chordBar
		if t1.t == 1 {
			if t2.t == 1 {
				t1.text = getChordBar(t1.text, true)
			} else {
				t1.text = getChordBar(t1.text, false)
			}
		}
		ret += t1.text + getSeparator(t1.t, t2.t)
	}

	last := tokens[len(tokens)-1]
	if last.t == 1 {
		last.text = getChordBar(last.text, true)
	}
	return ret + last.text
}

func isChord(word string) (string, bool) {
	if word[0] == '[' && word[len(word)-1] == ']' {
		return word[1 : len(word)-1], true
	}
	return "", false
}

// \,
func getSeparator(t1, t2 byte) string {
	if t1 == 0 && t2 == 0 {
		return ` `
	}
	if t1 == 0 && t2 == 1 {
		return ` `
	}
	if t1 == 0 && t2 == 2 {
		return ` `
	}
	if t1 == 1 && t2 == 0 {
		return ` `
	}
	if t1 == 1 && t2 == 1 {
		return ` `
	}
	if t1 == 1 && t2 == 2 {
		return ` `
	}
	if t1 == 2 && t2 == 0 {
		return `\,`
	}
	if t1 == 2 && t2 == 1 {
		return `\,`
	}
	if t1 == 2 && t2 == 2 {
		return ` `
	}
	panic("wrong types")
}

func parseChordWord(word string) string {
	if word[0] == '[' || word[0] == ';' || word[len(word)-1] == ';' || word[len(word)-1] == ']' {
		return "couldNotParseThisWord"
	}
	loadingChord := false
	chord := ""
	ret := ""
	for _, ch := range word {
		if loadingChord {
			if ch == ']' {
				loadingChord = false
				ret += "-" + getChordBar(chord, false) + "-"
				chord = ""
			} else {
				chord += string(ch)
			}
			continue
		}
		if ch == ';' {
			ret += getSmallBar()
			continue
		}
		if ch == '[' {
			loadingChord = true
			continue
		}
		ret += string(ch)
	}
	return ret
}
